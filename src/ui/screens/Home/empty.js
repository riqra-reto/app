import React from 'react';
import { Text, View, Button, StyleSheet, } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Riqra } from '../../styles';




export default class EmptyComponent extends React.Component {
    render() {
        return (
            <View style={{

                marginTop: 100,
                justifyContent: 'center',
                alignSelf: 'center',
                alignItems: 'center'
            }}>
                <Icon name="opencart" style={styles.icon} />

                <Text style={{ ...styles.title }}>Tu carrito de compras está vacio!</Text>
                <Text style={{ marginTop: 5 }}>Aún no has decido tu compra</Text>
            </View>


        );
    }
}



const styles = StyleSheet.create({
    icon: {
        fontSize: 45,
        textAlign: 'center'
    },
    title: {
        fontSize: 23,
        textAlign: 'center',
        fontWeight: '600',
        marginTop: 20
    }

})


