import React from 'react';
import { Text, View, Button, SafeAreaView } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';



import { Input, Header } from 'react-native-elements';



import EmptyComponent from './empty'



class HomeScreen extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            products: []
        }
    }

    static navigationOptions = {
        title: 'Riqra',
    };


    searchProduct(word) {
        //Busca la palabra

        console.log('Demostrando mi JS');

        let products = [{ id: 1, photo: 'https://lh5.googleusercontent.com/proxy/d-Xaf-gmHjZZH8wfqwUKTxxEHY4_jjHuf6LmZNxp8sHw8AU_eKy1wq2mp9w39xwAuyxk9MyM73qfk8U8qRaH-ySuxYC7og3mm-q7mRB6CVslSZVxdKJTGYQ48LtmIFHadr9gd_s', price: 21.1, name: 'Yogurt HDP' },
        { id: 2, photo: 'https://i1.wp.com/thesweetmolcajete.com/wp-content/uploads/2018/07/Desayuno-de-Quinoa-con-Yogurt-2.jpg?resize=610%2C915&ssl=1', price: 14.1, name: 'Batido HDP' }];

        this.setState({ products });

        console.log(products);
    }

    render() {
        return (

            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>

                <View style={{ backgroundColor: 'white' }}>



                    <View style={{ marginTop: 10 }}></View>

                    <Input
                        placeholder='Busca productos'
                        leftIcon={{ name: 'search' }}

                        onEndEditing={(e, x) => {
                            //buscamos
                            console.log('hola');

                            this.searchProduct('hola');
                        }}

                    />

                    <View style={{ marginTop: 20 }}></View>


                    <>

                        {this.state.products.length == 0 ? (<EmptyComponent />) : (<Text>Cargando productos </Text>)}
                    </>





                    {/*<Button
                    title="Go to Details"
                    onPress={() => this.props.navigation.navigate('Details')}
                />*/}
                </View>
            </SafeAreaView>
        );
    }
}


class DetailsScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Details!</Text>
            </View>
        );
    }
}

class SettingsScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                {/* other code from before here */}
                <Button
                    title="Go to Details"
                    onPress={() => this.props.navigation.navigate('Details')}
                />
            </View>
        );
    }
}

const HomeStack = createStackNavigator({
    Home: {
        screen: HomeScreen,

    },
    Details: DetailsScreen,
});

const SettingsStack = createStackNavigator({
    Settings: SettingsScreen,
    Details: DetailsScreen,
});

export default createAppContainer(
    createBottomTabNavigator(
        {
            Home: HomeStack,
            Settings: SettingsStack,
        },
        {
            /* Other configuration remains unchanged */
        }
    )
);