import { createStore, applyMiddleware ,combineReducers} from 'redux';
import product from './../modules/core/product/reducer';
import thunk from 'redux-thunk'



const Reducers = combineReducers({
    product
})


export default configureStore = () => {
    let store = createStore(Reducers, applyMiddleware(thunk))
    return store
}


