import axios from 'axios';


export async function getByName(name) {

    return axios.get('localhost:3000/api/' + 'products', {
        params: { name }, headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

}