import { FETCH_PRODUCTS } from './types';
import { getByName } from './../../services/product';


export const fetchProducts = (name) => async (dispatch) => {

    const products = await getByName(name);

    dispatch({
        type: FETCH_PRODUCTS,
        payload: products
    })
}
